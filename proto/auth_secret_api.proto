// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

syntax = "proto3";

package auth_secret_api;

option go_package = "./auth_secret_api";

message AuthSecretInfo {
  string username = 1;
  string password = 2;
}

service AuthSecretApi {
  //增加密钥
  rpc add(AddRequest) returns (AddResponse) {}
  //删除密钥
  rpc remove(RemoveRequest)
      returns (RemoveResponse) {}
  //更新密钥
  rpc update(UpdateRequest)
      returns (UpdateResponse) {}
  //获得单个密钥
  rpc get(GetRequest) returns (GetResponse) {}
  //列出密钥
  rpc list(ListRequest) returns (ListResponse) {}
  //列出密钥名称
  rpc listName(ListNameRequest) returns (ListNameResponse) {}
}

message AddRequest {
  string accessToken = 1;
  string remoteId = 2;
  AuthSecretInfo authSecret = 3;
}

message AddResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_EXIST_AUTH_SECRET = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message RemoveRequest {
  string accessToken = 1;
  string remoteId = 2;
  string username = 3;
}

message RemoveResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
  }
  CODE code = 1;
  string errMsg = 2;
}

message UpdateRequest {
  string accessToken = 1;
  string remoteId = 2;
  AuthSecretInfo authSecret = 3;
}

message UpdateResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_AUTH_SECRET = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message GetRequest {
  string accessToken = 1;
  string remoteId = 2;
  string username = 3;
}

message GetResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_AUTH_SECRET = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  AuthSecretInfo authSecret = 3;
}

message ListRequest {
  string accessToken = 1;
  string remoteId = 2;
}

message ListResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated AuthSecretInfo authSecretList = 3;
}

message ListNameRequest {
  string accessToken = 1;
  string remoteId = 2;
}

message ListNameResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated string nameList = 3;
}