// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

syntax = "proto3";

package image_api;

option go_package = "./image_api";

// 对应如下信息结构的公用信息
// application/vnd.docker.container.image.v1+json
// application/vnd.oci.image.config.v1+json
message ImageConfig {
  message Config {
    string user = 1;
    map<string, string> exposedPorts = 2;
    repeated string env = 3;
    repeated string entrypoint = 4;
    repeated string cmd = 5;
    map<string, string> volumes = 6;
    string workingDir = 7;
    map<string, string> labels = 8;
  }

  int64 created = 1;
  string author = 2;
  string architecture = 3;
  string os = 4;
  Config config = 5;
}

// 对应如下信息结构的公用信息
// application/vnd.docker.distribution.manifest.v2+json
// application/vnd.oci.image.manifest.v1+json
message ImageManifest {
  message Layer {
    string mediaType = 1;
    string digest = 2;
    uint32 size = 3;
  }

  ImageConfig config = 1;
  repeated Layer layers = 2;
}

// 对应如下信息结构的公用信息
// application/vnd.docker.distribution.manifest.list.v2+json
// application/vnd.oci.image.index.v1+json
message ImageManifestList {
  message ManifestItem {
    ImageManifest manifest = 1;
    string architecture = 2;
    string os = 3;
  }

  repeated ManifestItem manifests = 1;
}

message ImageReference {
  string groupName = 1;
  string imageName = 2;
  string reference = 3;  // tag 或digest
  ImageManifestList manifestInfo = 4;
  int64 timeStamp = 5;
  string digest = 6;
}

message ImageInfo {
  string groupName = 1;
  string imageName = 2;
  uint32 referenceCount = 3;
  int64 createTime = 4;  //创建时间
  int64 updateTime = 5;  //更新时间
}

service ImageApi {
  //列出镜像信息
  rpc listImage(ListImageRequest) returns (ListImageResponse) {}
  //列出所有镜像名称
  rpc listAllImage(ListAllImageRequest) returns (ListAllImageResponse) {}
  //获得单个镜像信息
  rpc getImage(GetImageRequest) returns (GetImageResponse) {}
  //删除镜像信息
  rpc removeImage(RemoveImageRequest) returns (RemoveImageResponse) {}
  //列出镜像Reference
  rpc listReference(ListReferenceRequest) returns (ListReferenceResponse) {}
  //获得单个镜像Reference
  rpc getReference(GetReferenceRequest) returns (GetReferenceResponse) {}
  //删除镜像Reference
  rpc removeReference(RemoveReferenceRequest)
      returns (RemoveReferenceResponse) {}
}

message ListImageRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  uint32 offset = 4;
  uint32 limit = 5;
}

message ListImageResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_GROUP = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  uint32 totalCount = 3;
  repeated ImageInfo imageList = 4;
}

message ListAllImageRequest {
  string accessToken = 1;
  string remoteId = 2;
}

message ListAllImageResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated ImageInfo imageList = 3;
}

message GetImageRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  string imageName = 4;
}

message GetImageResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_GROUP = 4;
    CODE_NO_IMAGE = 5;
  }
  CODE code = 1;
  string errMsg = 2;
  ImageInfo image = 3;
}

message RemoveImageRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  string imageName = 4;
  bool force = 5;
}

message RemoveImageResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_HAS_REFERENCE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message ListReferenceRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  string imageName = 4;
  bool loadManifest = 5;  //是否加载manifest信息
  bool includeDigest = 6; //tag和digest都加载

  uint32 offset = 10;
  uint32 limit = 11;
}

message ListReferenceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_GROUP = 4;
    CODE_NO_IMAGE = 5;
  }
  CODE code = 1;
  string errMsg = 2;
  uint32 totalCount = 3;
  repeated ImageReference referenceList = 4;
}

message GetReferenceRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  string imageName = 4;
  string reference = 5;
}

message GetReferenceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
    CODE_NO_GROUP = 4;
    CODE_NO_IMAGE = 5;
    CODE_NO_REFERENCE = 6;
  }
  CODE code = 1;
  string errMsg = 2;
  ImageReference reference = 3;
}

message RemoveReferenceRequest {
  string accessToken = 1;
  string remoteId = 2;
  string groupName = 3;
  string imageName = 4;
  string reference = 5;
}

message RemoveReferenceResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_ACCESS_TOKEN = 1;
    CODE_NO_PERMISSION = 2;
    CODE_NO_REMOTE = 3;
  }
  CODE code = 1;
  string errMsg = 2;
}