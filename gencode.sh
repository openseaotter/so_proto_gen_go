#!/bin/sh

# SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
# SPDX-License-Identifier: GPL-3.0-only

rm -rf auth_secret_api
protoc -I proto --go_out=. --go-grpc_out=. auth_secret_api.proto

rm -rf member_api
protoc -I proto --go_out=. --go-grpc_out=. member_api.proto

rm -rf image_group_api
protoc -I proto --go_out=. --go-grpc_out=. image_group_api.proto

rm -rf image_api
protoc -I proto --go_out=. --go-grpc_out=. image_api.proto

rm -rf config_api
protoc -I proto --go_out=. --go-grpc_out=. config_api.proto

rm -rf admin_api
protoc -I proto --go_out=. --go-grpc_out=. admin_api.proto

rm -rf watch_api
protoc -I proto --go_out=. --go-grpc_out=. watch_api.proto